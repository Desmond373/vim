
call plug#begin('~/.vim/plugged')

Plug 'ajh17/VimCompletesMe'

Plug 'scrooloose/syntastic'

Plug 'tpope/vim-surround'

Plug 'terryma/vim-multiple-cursors'

Plug 'altercation/vim-colors-solarized'

Plug 'dhruvasagar/vim-table-mode'

call plug#end()
set nocompatible

set mouse=a

set tabstop=4 shiftwidth=4 softtabstop=0 noexpandtab smarttab

set number relativenumber
set numberwidth=4
augroup numbertoggle
	autocmd!
	autocmd BufEnter,FocusGained,InsertLeave 	* set relativenumber
	autocmd BufLeave,FocusLost,InsertEnter 		* set norelativenumber
augroup END

syntax enable
set background=dark
colorscheme solarized

filetype plugin indent on
set omnifunc=syntaxcomplete#Complete

set tags+=~/.vim/tags/cpp
au BufNewFile,BufRead,BufEnter *.cpp,*.hpp set omnifunc=omni#cpp#complete#Main

set showcmd
set cursorline
set wildmenu
set lazyredraw
set incsearch
set hlsearch


set foldenable
set foldlevelstart=10
set foldnestmax=10
nnoremap <space> za
set foldmethod=indent
let mapleader=","

nnoremap <leader>ts :terminal<CR>
nnoremap <BS> :nohlsearch<CR>

set ruler

set path+=**

nnoremap ; :
nnoremap : ;

inoremap <silent> <Leader>fn <C-R>=expand("%:t:r")<CR>

set list
set showbreak=↪\
set listchars=tab:→\ ,nbsp:␣,trail:•,extends:⟩,precedes:⟨


let g:table_mode_corner='|'
